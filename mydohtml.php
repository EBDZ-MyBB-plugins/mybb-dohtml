<?php

/**
 * Changelog:
 * 1.0.0 : replace dohtml with style
 * 1.1.0 : allow line feed
 * 1.1.1 : fix line feed for less PHP broke
 * 1.1.2 : fix broke some post, still use validate_output
 * 1.2.0 : review order, do all mybb code before, disbale all ;line feed system …
 * 1.2.1 : review highlight order
 * 1.2.3 : css class with spécial character
 **/
$plugins->add_hook('parse_message_start', 'mydohtml_parse_message_start', -5);
$plugins->add_hook('parse_message_end', 'mydohtml_parse_message_end', 10); // 10 is the default.
$plugins->add_hook('parse_message_end', 'mydohtml_parse_message_final',20);


function mydohtml_parse_message_start($message) {
	global $dohtml;
	$dohtml = null;
	/* NO action needed */
	if(!preg_match('((\\[DOHTML\\])(.*?)(\\[/DOHTML\\]))is', $message, $matches)) {
		return $message;
	}
	$dohtml = array(
		'highlight' => '',
		'style' => '',
	);

	/* Remove style */
	preg_match_all('((\\[DOHTML\\])(.*?)(\\[/DOHTML\\]))is', $message, $matches, PREG_PATTERN_ORDER);
	if(isset($matches[2])) {
		foreach ($matches[2] as $matchedohtml) {
			preg_match_all('#<style.*?>(.*?)</style>#is', $matchedohtml, $stylematches, PREG_PATTERN_ORDER);
			if(isset($stylematches[1])) {
				foreach ($stylematches[1] as $addstyle) {
					if(is_string($addstyle)) {
						$dohtml['style'] .= $addstyle;
					}
				}
			}
		}
	}
	$message = preg_replace('#<style.*?>(.*?)</style>#is', '', $message);

	/* Remove all line feed */
	$message_new = preg_replace_callback('((\\[DOHTML\\])(.*?)(\\[/DOHTML\\]))is', function($matches) use ($dohtml) {
		$html = $matches[2];
		$html = str_replace("\n", ' ', $html);
		return '[DOHTML]' . preg_replace(array('~\R~u'), ' ', $html) . '[/DOHTML]';
	}, $message);
	/* deactivate highlight*/
	global $parser;
	if (is_object($parser))	{
		$dohtml['highlight'] = $parser->options['highlight'];
		$parser->options['highlight'] = '';
	}
	if ($message_new) {
		$message = $message_new;
	}

	return $message;
}

function mydohtml_parse_message_end($message)
{
	global $dohtml;
	/* NO action needed */
	if (empty($dohtml)) {
		return $message;
	}
	/* needed to disable 2 times cheking (and highlight)*/
	global $parser;
	$message = preg_replace_callback('((\\[DOHTML\\])(.*?)(\\[/DOHTML\\]))is', function($matches) {
		$dohtmltag = $matches[2];
		/* Some specific sapce broke in style */
		$dohtmltag = preg_replace('/\xc2\xa0/', ' ', $dohtmltag);
		$dohtmltag = html_entity_decode($dohtmltag, ENT_NOQUOTES);
		/* Line feed inline tag */
		$dohtmltag = str_replace("<linefeed>", "&#013;", $dohtmltag);
		return $dohtmltag;
	}, $message);
	$message = mydohtml_tidy_fixHTMLCode($message);
	return $message;




}
function mydohtml_parse_message_final($message)
{
	global $dohtml;
	/* NO action needed */
	if (empty($dohtml)) {
		return $message;
	}
	global $parser;
	if ($dohtml['highlight'] && is_object($parser)) {
		$message = $parser->highlight_message($message, $dohtml['highlight']);
	}
	/* Don't parse before dohtml */
	$dohtmlstyle = strip_tags($dohtml['style']);

	if ($dohtmlstyle) {
		$dohtmlstyle = preg_replace('/\xc2\xa0/', ' ', $dohtmlstyle);
		$dohtmlstyle = htmlspecialchars($dohtmlstyle);
		$dohtmlstyle = str_replace(" &gt; "," > ",$dohtmlstyle);
	}
	if ($dohtmlstyle) {
		$message = "<style>" . $dohtmlstyle . "</style>" . $message;
	}
	if (is_object($parser)) {
		$parser->options['allow_html'] = 1;
	}
	$result = mydohtml_checkXMLerror($message);
	if ($result) {
		$backup = $message;
		$message = "<strong>Errors : </strong><pre>" . $result . "</pre>";
		$message.= "<strong>Final :  </strong><pre>" . htmlentities($backup) . "</pre>";
	}
	$dohtml = null;
	return $message;
}
function mydohtml_info() {
	return array(
		'name'          => '[DOHTML] Tag',
		'description'   => 'Allow usage of DOHTML in forum without html.',
		'compatibility' => '18*',
		'author'        => 'Betty',
		'authorsite'    => '',
		'version'       => '1.2.0',
		'codename'      => 'mydohtml',
	);
}

function mydohtml_tidy_fixHTMLCode($html) {
	$tidy_options = array (
		'clean' => 0, // https://api.html-tidy.org/tidy/quickref_5.8.0.html#clean
		'logical-emphasis' => 1,
		'drop-empty-elements' => 0,
		'drop-empty-paras' => 0,
		'drop-proprietary-attributes' => 0,
		'fix-backslash' => 1,
		'hide-comments' => 1,
		'join-styles' => 1,
		'lower-literals' => 1,
		'merge-divs' => 0,
		'merge-spans' => 0,
		'output-xhtml' => 1,
		'word-2000' => 0,
		'wrap' => 0,
		'output-bom' => 0,
		'show-body-only'=>true,
		'indent'=>true
		//~ 'char-encoding' => 'utf8',
		//~ 'input-encoding' => 'utf8',
		//~ 'output-encoding' => 'utf8'
	);

	// clean up the HTML code
	$tidy = tidy_parse_string($html, $tidy_options);
	//~ // fix the HTML
	$tidy->cleanRepair();
	//~ // get the body part
	$tidy_body = tidy_get_body($tidy);
	$html = $tidy_body->value;
	// fix some self-closing tags
	$html = str_replace(array('<body>','</body>'),'', $html);

	// return the cleaned XHTML code
	return $html;
}

function  mydohtml_checkXMLerror($output)
{
		$ignored_error_codes = array(
			// entities may be broken through smilie parsing; cache_smilies() method workaround doesn't cover all entities
			'XML_ERR_INVALID_DEC_CHARREF' => 7,
			'XML_ERR_INVALID_CHAR' => 9,

			'XML_ERR_UNDECLARED_ENTITY' => 26, // unrecognized HTML entities
			'XML_ERR_ATTRIBUTE_WITHOUT_VALUE' => 41,
			'XML_ERR_TAG_NAME_MISMATCH' => 76, // the parser may output tags closed in different levels and siblings
		);

		libxml_use_internal_errors(true);
		libxml_disable_entity_loader(true);
		simplexml_load_string('<root>'.$output.'</root>', 'SimpleXMLElement', 524288 /* LIBXML_PARSEHUGE */);

		$errors = libxml_get_errors();
		libxml_use_internal_errors(false);
		if(
			$errors &&
			array_diff(
				array_column($errors, 'code'),
				$ignored_error_codes
			)
		)
		{
			return print_r($errors,1);
		}
}
